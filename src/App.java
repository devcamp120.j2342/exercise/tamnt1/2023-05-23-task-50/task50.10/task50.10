package src;

public class App {
    public static void main(String[] args) throws Exception {
        sumNumbersV1();
        sumNumbersV2(new int[] { 1, 2, 3, 5, 7, 9 });
        printHello(24);
        printHello(99);
    }

    public static void sumNumbersV1() {
        int sum = 0;
        for (int i = 1; i <= 100; i++) {
            sum += i;
        }
        System.out.println(sum);
    }

    public static void sumNumbersV2(int[] numbers) {
        int sum = 0;
        for (int number : numbers) {
            sum += number;
        }
        System.out.println(sum);
    }

    public static void printHello(int number) {
        if (number % 2 == 0) {
            System.out.println("Đây là số chẵn");
        } else {
            System.out.println("Đây không là số chẵn");
        }
    }

}
